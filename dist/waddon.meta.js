// ==UserScript==
// @name        Waddon
// @description General purpose addon for Isleward
// @namespace   Isleward.Waddon
// @author      hazel
// @match       https://play.isleward.com/*
// @match       https://ptr.isleward.com/*
// @downloadURL https://gitlab.com/Isleward-Modding/addons/waddon/-/raw/master/dist/waddon.user.js
// @updateURL   https://gitlab.com/Isleward-Modding/addons/waddon/-/raw/master/dist/waddon.meta.js
// @version     3.4.12
// @grant       GM_xmlHttpRequest
// ==/UserScript==
