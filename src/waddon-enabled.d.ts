// See scripts/waddon-features-list.js

declare module 'waddon-enabled' {
	const list: import('./features').WaddonFeature[];
	export = list;
}
