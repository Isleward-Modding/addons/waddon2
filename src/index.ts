/* eslint-disable */

import { addon } from './addon';

// Load with AddonManager or IWD's window.addons
// See here: https://gitlab.com/Isleward-Modding/addons/addonmanager#addon-development

// prettier-ignore
// @ts-expect-error
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})(addon);
