import { defaultRulesEnabled } from './features/minimap/rules';
import { VISUAL_INITIAL } from './features/visual';

const PREFIX = 'waddon.';

export interface WaddonSettings {
	'timers.prettifyTicks': boolean;
	'timers.cache': Record<string, number>;
	'timers.enabled': boolean;
	'addonMenu.misc.logLinkedItems': boolean;
	'addonMenu.misc.hideMenuButton': boolean;
	version: string | undefined;
	'islebuilds.authToken': string | undefined;
	'islebuilds.uploadPeriod': number;
	'islebuilds.announceUploads': boolean;
	'islebuilds.syncInventory': boolean;
	'islebuilds.lastUpload': Record<string, number>;
	'islebuilds.chatNames': number;
	'islebuilds.chatNamesCache': Record<string, string>;
	renderer: typeof VISUAL_INITIAL;
	'location.enabled': boolean;
	'farm.enabled': boolean;
	'xp.enabled': boolean;
	'qol.hideQuests': boolean;
	'qol.chatReplay': boolean;
	'qol.playerContext': boolean;
	'minimap.enabled': boolean;
	'minimap.rules': Record<string, boolean>;
	'minimap.logObjects': boolean;
	'minimap.scale': number;
	'tooltips.enabled': boolean;
	'tooltips.cardSetSize': boolean;
	'tooltips.shortenStatNames': boolean;
	'tooltips.perfection.maxLevel': boolean;
	'tooltips.perfection.maxLevel23': boolean;
	'tooltips.perfection.stats': boolean;
	'tooltips.perfection.spells': boolean;
	'tooltips.perfection.showPercent': boolean;
	'tooltips.perfection.showRange': boolean;
	'tooltips.perfection.showOverall': boolean;
	'charselect.enabled': boolean;
}

const defaults: {
	[T in keyof WaddonSettings]: WaddonSettings[T];
} = {
	'timers.prettifyTicks': true,
	'timers.cache': {},
	'timers.enabled': false,
	'addonMenu.misc.logLinkedItems': false,
	'addonMenu.misc.hideMenuButton': false,
	version: undefined,
	'islebuilds.authToken': undefined,
	'islebuilds.uploadPeriod': -1,
	'islebuilds.announceUploads': true,
	'islebuilds.syncInventory': false,
	'islebuilds.lastUpload': {},
	'islebuilds.chatNames': 0,
	'islebuilds.chatNamesCache': {},
	renderer: VISUAL_INITIAL,
	'location.enabled': false,
	'farm.enabled': false,
	'xp.enabled': false,
	'qol.hideQuests': false,
	'qol.chatReplay': false,
	'qol.playerContext': false,
	'minimap.enabled': false,
	'minimap.rules': defaultRulesEnabled,
	'minimap.logObjects': false,
	'minimap.scale': 1,
	'tooltips.enabled': false,
	'tooltips.cardSetSize': true,
	'tooltips.shortenStatNames': false,
	'tooltips.perfection.maxLevel': true,
	'tooltips.perfection.maxLevel23': false,
	'tooltips.perfection.stats': true,
	'tooltips.perfection.spells': true,
	'tooltips.perfection.showPercent': true,
	'tooltips.perfection.showRange': false,
	'tooltips.perfection.showOverall': false,
	'charselect.enabled': false,
};

type KeysMatching<T, V> = {
	[K in keyof T]-?: T[K] extends V ? K : never;
}[keyof T];

export type SettingsMatching<V> = KeysMatching<WaddonSettings, V>;

type ObserverType<T extends keyof WaddonSettings> = (
	value: WaddonSettings[T],
) => void;
const observers: {
	[T in keyof WaddonSettings]?: ObserverType<T>[];
} = {};

const onKeyChanged = <T extends keyof WaddonSettings>(
	key: T,
	cb: ObserverType<T>,
) => {
	let oList = observers[key];
	if (!oList) {
		oList = [];
		observers[key] = oList;
	}

	oList.push(cb);

	// Call with current data
	cb(getValue(key));
};
const offKeyChanged = <T extends keyof WaddonSettings>(
	key: T,
	cb: ObserverType<T>,
) => {
	const oList = observers[key];
	if (!oList) return;

	// @ts-expect-error TS can't verify that the types for observers[T] matches observers[T].filter
	observers[key] = oList.filter((o) => o !== cb);
};

const getValue = <T extends keyof WaddonSettings>(
	key: T,
): WaddonSettings[T] => {
	const persistedValue = localStorage.getItem(PREFIX + key);
	if (persistedValue === null) {
		return getDefaultValue(key);
	} else {
		try {
			return JSON.parse(persistedValue);
		} catch (e) {
			return persistedValue as any;
		}
	}
};

const stringify = (d: any) => {
	return d === undefined || typeof d === 'function'
		? d + ''
		: JSON.stringify(d);
};

const setValue = <T extends keyof WaddonSettings>(
	key: T,
	value: WaddonSettings[T],
) => {
	localStorage.setItem(PREFIX + key, stringify(value));

	const oList = observers[key];
	if (oList) {
		oList.forEach((o) => o(value));
	}
};

const transactValue = <T extends keyof WaddonSettings>(
	key: T,
	fn: (data: WaddonSettings[T]) => WaddonSettings[T],
) => {
	const prev = getValue(key);
	const transacted = fn(prev);
	setValue(key, transacted);

	const oList = observers[key];
	if (oList) {
		oList.forEach((o) => o(getValue(key)));
	}
};

const getDefaultValue = <T extends keyof WaddonSettings>(
	key: T,
): WaddonSettings[T] => {
	return defaults[key];
};

export const settings = {
	onKeyChanged,
	offKeyChanged,
	getValue,
	setValue,
	transactValue,
	defaults,
};
