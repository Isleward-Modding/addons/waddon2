import { checkVersion } from './util/checkVersion';
import { getWindow } from './util/getWindow';
import { hello } from './util/hello';
import { sendMessage } from './util/sendMessage';

// Features is really the only 100% core module
import { features } from './features';

// Many features depend on these
import renderer from './core/renderer';
import slash from './core/slash';
import commands from './core/commands';
import menu from './core/menu';
import state from './core/state';
import hotkeys from './core/hotkeys';

// Called by Isleward addons module
const init = async () => {
	hello();

	// Core features
	features.register(renderer);
	features.register(state);
	features.register(commands);
	features.register(slash);
	features.register(menu);
	features.register(hotkeys);

	// Init features
	// Also registers non-core features
	await features.init();

	checkVersion();
};

const addon = {
	// For AM
	id: typeof ADDON_ID === 'undefined' ? 'waddon' : ADDON_ID,
	name: 'Waddon',

	init,

	// Expose
	registerAction: slash.registerAction,
	unregisterAction: slash.unregisterAction,

	sendMessage: sendMessage,
};

// Expose for other addons
getWindow().Waddon = addon;

export { addon };
