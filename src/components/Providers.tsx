import { PanelOrder } from '@kckckc/isleward-util';
import React, { type ReactNode, useState } from 'react';

interface ProvidersProps {
	children?: ReactNode;
}

export const Providers: React.FC<ProvidersProps> = ({ children }) => {
	const [orderValue, setOrderValue] = useState(30);

	return (
		<PanelOrder.Provider
			value={{ value: orderValue, setValue: setOrderValue }}
		>
			{children}
		</PanelOrder.Provider>
	);
};
