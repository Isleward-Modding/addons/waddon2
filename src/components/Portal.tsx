import { useState, useEffect, type ReactNode } from 'react';
import { createPortal } from 'react-dom';

interface PortalProps {
	children?: ReactNode;
	selector?: string;
	prepend?: boolean;
	className?: string;
}

export const Portal: React.FC<PortalProps> = ({
	children,
	selector = 'body',
	prepend = false,
	className = '',
}) => {
	const [container] = useState(() => {
		const el = document.createElement('div');
		el.className = className;
		return el;
	});

	useEffect(() => {
		const target = document.querySelector(selector);

		if (!target) return;

		if (prepend) {
			target.prepend(container);
		} else {
			target.append(container);
		}

		return () => {
			document.querySelector(selector)?.removeChild(container);
		};
	}, [container, selector, prepend]);

	return createPortal(children, container);
};
