import { useEffect, useState } from 'react';
import state, { type GameState } from './index';

const useGameState = () => {
	const [value, setValue] = useState<GameState>('login');

	useEffect(() => {
		const cb = (newState: GameState) => {
			setValue(newState);
		};

		state.subscribe(cb);
		return () => {
			state.unsubscribe(cb);
		};
	});

	return value;
};

export { useGameState };
