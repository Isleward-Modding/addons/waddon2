import { CM, ContextMenu, Portal as CMPortal } from '@kckckc/isleward-util';
import React, { useEffect, useRef, useState } from 'react';
import { Portal } from '../../components/Portal';
import { useSetting } from '../../hooks/useSetting';
import { Menu } from './Menu';
import styles from './styles.module.css';

// External toggler
type Listener = () => void;
let toggleListeners: Listener[] = [];
export const toggle = () => {
	toggleListeners.forEach((l) => l());
};

// Show/hide logic
const MenuController = ({
	show,
	passRef,
	pos,
}: {
	show: boolean;
	passRef: React.RefObject<HTMLDivElement>;
	pos: { x: number; y: number };
}) => {
	return (
		<>
			{show && (
				<CMPortal>
					<ContextMenu ref={passRef} pos={pos}>
						<Menu />
					</ContextMenu>
				</CMPortal>
			)}
		</>
	);
};

// No button
export const HiddenButton = () => {
	// Ref for checking if we click outside menu
	const ref = useRef<HTMLDivElement>(null);

	const [showMenu, setShowMenu] = useState(false);

	// Close the menu if we click outside
	useEffect(() => {
		const onClick = (e: MouseEvent) => {
			// If the context menu is visible and we click outside of it
			if (
				showMenu &&
				e.target instanceof Node &&
				!ref.current?.contains(e.target)
			) {
				setShowMenu(false);
			}
		};

		if (showMenu) {
			document.addEventListener('mousedown', onClick);
		}

		return () => {
			document.removeEventListener('mousedown', onClick);
		};
	}, [showMenu, ref]);

	// Listen for toggle messages (sent by hotkey manager)
	useEffect(() => {
		const onToggle = () => {
			setShowMenu((s) => !s);
		};

		toggleListeners.push(onToggle);

		return () => {
			toggleListeners = toggleListeners.filter((t) => t !== onToggle);
		};
	}, []);

	return (
		<MenuController pos={{ x: 16, y: 16 }} show={showMenu} passRef={ref} />
	);
};

// Styled & interactive button
const MainMenuButton = () => {
	// Ref for getting button bounding rect
	const buttonRef = useRef<HTMLDivElement>(null);
	// Ref for checking if we click outside menu
	const ref = useRef<HTMLDivElement>(null);

	// Show menu on click
	const [showMenu, setShowMenu] = useState(false);
	const onClick: React.MouseEventHandler<HTMLDivElement> = (e) => {
		if (showMenu) {
			setShowMenu(false);
			return;
		}
		setShowMenu(true);
		e.preventDefault();
	};

	// Close the menu if we click outside
	useEffect(() => {
		const onClick = (e: MouseEvent) => {
			// If the context menu is visible and we click outside of it and outside the button
			if (
				showMenu &&
				e.target instanceof Node &&
				!ref.current?.contains(e.target) &&
				!buttonRef.current?.contains(e.target)
			) {
				setShowMenu(false);
			}
		};

		if (showMenu) {
			document.addEventListener('mousedown', onClick);
		}

		return () => {
			document.removeEventListener('mousedown', onClick);
		};
	}, [showMenu, ref]);

	// Listen for toggle messages (sent by hotkey manager)
	useEffect(() => {
		const onToggle = () => {
			setShowMenu((s) => !s);
		};

		toggleListeners.push(onToggle);

		return () => {
			toggleListeners = toggleListeners.filter((t) => t !== onToggle);
		};
	}, []);

	// Calculate menu position
	let pos = { x: 0, y: 0 };
	if (buttonRef.current) {
		const r = buttonRef.current?.parentElement?.getBoundingClientRect();
		if (r) {
			pos = {
				x: r.x - 2,
				y: r.y + r.height,
			};
		}
	}

	return (
		<>
			<div ref={buttonRef} onClick={onClick} className={styles.icon} />

			<MenuController pos={pos} show={showMenu} passRef={ref} />
		</>
	);
};

// Floating button
export const ButtonInBody = () => {
	return (
		<div
			className={`hasBorderShadow ${styles.container} ${styles.floating}`}
		>
			<MainMenuButton />
		</div>
	);
};

// Add button to buffs ui
export const ButtonInBuffs = () => {
	return (
		<Portal
			className={`waddon-main-menu-button hasBorderShadow ${styles.container}`}
			selector=".uiEffects"
			prepend
		>
			<MainMenuButton />
		</Portal>
	);
};

export const HideMenuCheckbox = () => {
	const [hideMenuButton, setHideMenuButton] = useSetting(
		'addonMenu.misc.hideMenuButton',
	);

	return (
		<CM.Checkbox
			checked={hideMenuButton ?? false}
			onChange={() => {
				setHideMenuButton((prev) => {
					if (!prev) {
						alert(
							'Waddon menu button hidden, use hotkey to access',
						);
					}
					return !prev;
				});
			}}
		>
			hide menu button
		</CM.Checkbox>
	);
};
