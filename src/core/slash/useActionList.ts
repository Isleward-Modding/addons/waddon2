import { useEffect, useState } from 'react';
import slash, { type SlashAction } from './index';

export const useActionList = () => {
	const [actionList, setActionList] = useState<SlashAction[]>([]);

	useEffect(() => {
		const cb = (list: SlashAction[]) => setActionList(list);

		slash.subscribe(cb);
		return () => {
			slash.unsubscribe(cb);
		};
	}, []);

	return actionList;
};
