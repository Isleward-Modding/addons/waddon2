import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useEvents } from '../../hooks/useEvents';
import type { SlashAction } from './index';
import styles from './styles.module.css';
import { useActionList } from './useActionList';

export const Slashbar: React.FC = () => {
	const events = useEvents();

	const actions = useActionList();

	const [show, setShow] = useState(false);

	const [filteredActions, setFilteredActions] = useState<SlashAction[]>([]);
	const [activeActionIndex, setActiveActionIndex] = useState(0);
	const [showActions, setShowActions] = useState(false);
	const [input, setInput] = useState('');

	const hide = () => {
		setFilteredActions([]);
		setInput('');
		setActiveActionIndex(0);
		setShowActions(false);
		setShow(false);
	};
	const triggerAction = (a: SlashAction) => {
		if (a && a.callback) {
			a.callback();
		} else {
			if (a) {
				console.log(
					'Waddon Slash: Missing action callback for ' + a.name,
					a,
				);
			}
		}
	};

	const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const userInput = e.target.value;

		if (userInput == '') {
			setInput(userInput);
			setFilteredActions([]);
			setActiveActionIndex(0);
			setShowActions(false);
			return;
		}

		// filter suggestions
		const filtered = actions.filter(
			(a) => a.name.toLowerCase().indexOf(userInput.toLowerCase()) > -1,
		);

		setInput(userInput);
		setFilteredActions(filtered);
		setActiveActionIndex(0);
		setShowActions(true);
	};

	const onActionClick = (a: SlashAction, e: React.MouseEvent) => {
		triggerAction(a);
		hide();
		e.preventDefault();
	};

	const onKeyDown = (e: React.KeyboardEvent) => {
		if (e.key == 'Enter') {
			const a = filteredActions[activeActionIndex];
			if (a) {
				triggerAction(a);

				// Allow chaining
				if (e.shiftKey) {
					setFilteredActions([]);
					setInput('');
					setActiveActionIndex(0);
					setShowActions(false);
				} else {
					hide();
				}
			} else if (input === '') {
				hide();
			}
		} else if (e.key == 'ArrowUp') {
			if (activeActionIndex > 0) {
				setActiveActionIndex(activeActionIndex - 1);
			}
		} else if (e.key == 'ArrowDown') {
			if (activeActionIndex < filteredActions.length - 1) {
				setActiveActionIndex(activeActionIndex + 1);
			}
		} else if (e.key == 'Escape') {
			hide();
		}
	};

	// Listen for '/' key and open+focus search bar
	useEffect(() => {
		const onKeyDown = (e: KeyboardEvent) => {
			// Only open when nothing is focused (document.body)
			if (!e || e.target !== document.body) return;

			if (e.key === '/') {
				setShow(true);

				// preventDefault so that the / key doesn't get typed in the search bar
				e.preventDefault();
			}
		};
		const onClick = () => {
			hide();
		};

		document.body.addEventListener('keydown', onKeyDown);
		document.body.addEventListener('click', onClick);

		return () => {
			document.body.removeEventListener('keydown', onKeyDown);
			document.body.removeEventListener('click', onClick);
		};
	}, [events]);

	if (!show) return null;

	return (
		<div className={styles.container}>
			<input
				className={classNames(styles.search, 'hasBorderShadow')}
				type="text"
				placeholder="start typing to search actions..."
				spellCheck="false"
				autoFocus
				onChange={onChange}
				onKeyDown={onKeyDown}
				value={input}
			/>
			{showActions &&
				input &&
				(filteredActions.length ? (
					<ul className={classNames(styles.list, 'hasBorderShadow')}>
						{filteredActions.map((a, i) => {
							let className;
							if (i == activeActionIndex) {
								className = styles.active;
							}
							return (
								<li
									className={classNames(
										className,
										styles.item,
									)}
									key={a.id}
									onClick={(e) => onActionClick(a, e)}
								>
									{a.name}
								</li>
							);
						})}
					</ul>
				) : (
					<div
						className={classNames(
							styles['no-suggestions'],
							'hasBorderShadow',
						)}
					>
						No actions found!
					</div>
				))}
		</div>
	);
};
