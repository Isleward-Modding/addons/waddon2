import React from 'react';
import renderer from '../../core/renderer';
import { getDefaultActions } from './actions';
import { Slashbar } from './Slashbar';

// Types
export interface SlashAction {
	id: string;
	name: string;
	callback: () => void;
}
type SlashActionListener = (list: SlashAction[]) => void;

// State
let actions: SlashAction[] = [];
let listeners: SlashActionListener[] = [];

// Subscriptions
const subscribe = (cb: SlashActionListener) => {
	listeners.push(cb);

	cb(actions);
};
const unsubscribe = (cb: SlashActionListener) => {
	listeners = listeners.filter((l) => l !== cb);
};
const fireListeners = () => {
	listeners.forEach((l) => l(actions));
};

// Actions
const registerAction = (action: SlashAction) => {
	if (actions.find((a) => a.id === action.id)) {
		console.log(
			'Waddon Slash: Attempted to register duplicate action ' + action.id,
		);
		return;
	}

	actions.push(action);

	fireListeners();
};
const unregisterAction = (action: SlashAction) => {
	actions = actions.filter((a) => a !== action);

	fireListeners();
};

// Lifecycle
const init = async () => {
	const defaultActions = await getDefaultActions();

	defaultActions.forEach((a) => registerAction(a));

	renderer.addElement('slash', <Slashbar />);
};

const slash = {
	layer: 'core',

	init,
	registerAction,
	unregisterAction,
	subscribe,
	unsubscribe,
};

export default slash;
