import type { IwdChatMessage, IwdGlobalObjectList } from '../../isleward.types';
import { settings } from '../../settings';
import { getWindow } from '../../util/getWindow';
import { sendPostRequest } from '../../util/sendHttpRequest';

const triggers = [
	/(\w+) has come online/,
	/(\w+) has gone offline/,
	/(\w+) has joined the party/,
	/(\w+) has left the party/,
];

const onGetMessages = ({ messages }: { messages: IwdChatMessage[] }) => {
	const format = settings.getValue('islebuilds.chatNames');
	if (format == 0) return;

	if (!messages) return;

	messages.forEach((m) => {
		if (!m.message) return;

		if (m.source) {
			const displayName = getCachedName(m.source);
			if (!displayName) return;

			if (displayName) {
				const parts = m.message
					.replace(/ (?![^<>]*>)/, '&nbsp;')
					.split(':');
				const idx = m.message.startsWith('(party:') ? 1 : 0;
				parts[idx] = parts[idx].replace(
					m.source,
					formatName(m.source, displayName),
				);
				m.message = parts.join(':');
			}
		} else if (m.message && !m.message.includes(':')) {
			for (let i = 0; i < triggers.length; i++) {
				const pattern = triggers[i];
				const found = m.message.match(pattern);
				if (!found) continue;

				m.message = m.message.replace(
					pattern,
					(match, p1, offset, string) => {
						const displayName = getCachedName(p1);
						const replacement = displayName
							? formatName(p1, displayName)
							: p1;

						return string
							.replace(/ (?![^<>]*>)/, '&nbsp;')
							.replace(p1, replacement);
					},
				);
			}
		}
	});
};

const formatName = (charName: string, displayName: string) => {
	const format = settings.getValue('islebuilds.chatNames');

	if (format == 0) {
		return charName;
	} else if (format == 1) {
		return `${charName} (${displayName})`;
	} else {
		return `<span onMouseOver="this.innerHTML='${charName}'" onMouseOut="this.innerHTML='${displayName}'">${displayName}</span><span class="color-grayA">*</span>`;
	}
};

const getCachedName = (char: string) => {
	const cache = settings.getValue('islebuilds.chatNamesCache');

	return cache[char];
};

const characterBuffer: string[] = [];
let fetchDebounce: ReturnType<typeof setTimeout> | null = null;
const debounceTimeout = 2000;
const queueFetchName = (character: string) => {
	characterBuffer.push(character);

	// Debounce call to fetchNames
	if (fetchDebounce) {
		clearTimeout(fetchDebounce);
	}
	fetchDebounce = setTimeout(fetchNames, debounceTimeout);
};

const fetchNames = async () => {
	const list = characterBuffer.splice(0);

	const response = await sendPostRequest('/api/nicknames', {
		characters: list,
	});
	const data = JSON.parse(response);

	const cache = settings.getValue('islebuilds.chatNamesCache');
	settings.setValue('islebuilds.chatNamesCache', { ...cache, ...data });
};

const globalObjectListUpdated = (globalObjects: IwdGlobalObjectList) => {
	globalObjects.list.forEach((c) => queueFetchName(c.name));
};

export const nicknamesInit = () => {
	const format = settings.getValue('islebuilds.chatNames');
	if (!format) {
		return;
	}

	getWindow().addons?.events?.on('onGetMessages', onGetMessages);
	getWindow().addons?.events?.on(
		'globalObjectListUpdated',
		globalObjectListUpdated,
	);
};
