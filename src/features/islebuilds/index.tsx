import { Button } from '@kckckc/isleward-util';
import React from 'react';
import { makePopup, Popup } from '../../components/Popup';
import commandsModule from '../../core/commands';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import slash from '../../core/slash';
import { settings } from '../../settings';
import { getPlayer, getWindow } from '../../util/getWindow';
import { sendGetRequest } from '../../util/sendHttpRequest';
import {
	uploadCharacter,
	uploadInventory,
	uploadStash,
} from '../islebuilds/islebuildsApi';
import { IslebuildsMenu } from './IslebuildsMenu';
import { nicknamesInit } from './nicknames';

const getLastUpload = (name: string) => {
	const lastUpload = settings.getValue('islebuilds.lastUpload');

	return lastUpload[name] ?? 0;
};

const updateLastUpload = (name: string) => {
	const lu = settings.getValue('islebuilds.lastUpload');
	lu[name] = Date.now();
	settings.setValue('islebuilds.lastUpload', lu);
};

const checkAutoUpload = () => {
	const player = getPlayer();
	const name = player?.name;
	if (!name) return;

	const authToken = settings.getValue('islebuilds.authToken');
	const uploadPeriod = settings.getValue('islebuilds.uploadPeriod');
	const announceUploads = settings.getValue('islebuilds.announceUploads');

	// Try often, will check if unnecessary
	uploadInventory();

	// Character
	if (
		authToken &&
		authToken !== '' &&
		uploadPeriod > -1 &&
		new Date().getTime() - getLastUpload(name) >= uploadPeriod
	) {
		uploadCharacter({ noOpen: true, noMessage: !announceUploads });
	}
};

const fetchToken = async (otp: string) => {
	try {
		const response = await sendGetRequest('/api/otp', {
			Authorization: `Bearer ${otp}`,
		});
		if (response === 'expired') {
			makePopup(
				'Islebuilds failed to connect',
				'The token was invalid or expired. Please try again.',
			);
			return;
		}
		settings.setValue('islebuilds.authToken', response);
	} catch (e) {
		makePopup('Islebuilds setup failed', "Couldn't connect to Islebuilds");
		return;
	}

	const canEnableAuto = settings.getValue('islebuilds.uploadPeriod') === -1;

	renderer.addElement(
		'islebuilds-wizard',
		<Popup id="islebuilds-wizard" header={`Islebuilds connected`}>
			<div style={{ marginBottom: '12px' }}>
				You&apos;re now able to upload characters to Islebuilds!
			</div>

			<div style={{ marginBottom: '12px' }}>
				If you have other Isleward tabs open, they might need to be
				refreshed.
			</div>

			<div style={{ marginBottom: '12px' }}>
				Use <span className="q4">/upload</span> while in-game to upload
				your character.
			</div>
			{canEnableAuto && (
				<div style={{ marginBottom: '12px' }}>
					Would you like to enable auto-uploads to keep
					<br />
					your characters up-to-date on Islebuilds?
				</div>
			)}
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					columnGap: '12px',
				}}
			>
				<div style={{ flex: 1 }} />
				{canEnableAuto && (
					<Button
						onClick={() => {
							settings.setValue(
								'islebuilds.uploadPeriod',
								1000 * 60 * 30,
							);
							renderer.removeElement('islebuilds-wizard');
						}}
					>
						Enable Auto-Upload
					</Button>
				)}
				<Button
					negative
					onClick={() => {
						renderer.removeElement('islebuilds-wizard');
					}}
				>
					Close
				</Button>
			</div>
		</Popup>,
	);
};

const init = () => {
	// Slash action
	slash.registerAction({
		id: 'islebuilds:upload',
		name: 'Islebuilds: Upload Character',
		callback: () => {
			uploadCharacter();
		},
	});

	// Chat command
	commandsModule.register('/upload', (msgConfig: { success: boolean }) => {
		// Close the chat
		msgConfig.success = false;

		uploadCharacter();
	});

	// Menu options
	menu.addElement({
		key: 'islebuilds',
		el: <IslebuildsMenu />,
	});

	// Check query params
	const params = new URLSearchParams(
		new URL(document.location.toString()).searchParams,
	);
	if (params.has('islebuilds')) {
		getWindow().history.replaceState(
			null,
			'',
			getWindow().location.pathname,
		);

		fetchToken(params.get('islebuilds') ?? '');
	}

	// Check for uploads every 5s
	setInterval(checkAutoUpload, 5000);

	// Bind to events
	const events = getWindow()?.addons?.events;
	if (events) {
		events.on('onGetItems', () => {
			setTimeout(uploadInventory, 50);
		});
		events.on('onDestroyItems', () => {
			setTimeout(uploadInventory, 50);
		});
		events.on('onGetMap', () => {
			setTimeout(uploadInventory, 50);
		});

		events.on('onOpenStash' as any, () => {
			setTimeout(uploadStash, 50);
		});
		events.on('onAddStashItems' as any, () => {
			setTimeout(uploadStash, 50);
		});
		events.on('onRemoveStashItems' as any, () => {
			setTimeout(uploadStash, 50);
		});
	}

	nicknamesInit();
};

export default { init, updateLastUpload };
