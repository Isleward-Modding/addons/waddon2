import { Button } from '@kckckc/isleward-util';
import type { IWDItem } from '@kckckc/isleward-util/isleward';
import React from 'react';
import { ISLEBUILDS_WEB_URL } from '../../config';
import { settings } from '../../settings';
import { getPlayer, getWindow } from '../../util/getWindow';
import { sendPostRequest } from '../../util/sendHttpRequest';
import islebuilds from './index';
import { makePopup } from '../../components/Popup';

export const uploadCharacter = async ({
	noMessage = false,
	noOpen = false,
} = {}) => {
	// Check site
	if (!isValidHost()) return;

	const player = getPlayer();

	if (!player?.name) return;

	// Set lastUploaded prop for auto upload interval
	// Do this now, because we shouldn't keep spamming error messages
	//   when token is invalid or api is down
	islebuilds.updateLastUpload(player.name);

	const token = settings.getValue('islebuilds.authToken');
	if (!token || token === '') {
		makePopup(
			'Islebuilds upload failed',
			<div style={{ marginBottom: '12px' }}>
				You need to log in to Islebuilds to upload characters.
			</div>,
			<Button
				onClick={() => {
					getWindow().open(
						getIslebuildsPagePath('/waddon/connect'),
						'_blank',
					);
				}}
			>
				Finish Setup
			</Button>,
		);
		console.log('[islebuilds] upload failed. token not set');
		return;
	}

	// Select some info from window.player
	const data = {
		name: player.name,
		level: player.level,
		league: player.league.leagueId,
		spirit: player.class,
		passives: player.passives.selected,
		portrait: player.portrait,
		skinId: player.skinId,
		skinSheetName: undefined as undefined | string,
		skinCell: undefined as undefined | number,
	};

	// add skin if not mounted
	if (!player.effects.effects.some((e) => e.type == 'mounted')) {
		data.skinSheetName = player.sheetName;
		data.skinCell = player.cell;
	}

	const rep: Record<string, number> = {};
	player.reputation.list.forEach((f) => {
		rep[`rep_${f.id}`] = f.rep;
	});

	// Equipment
	const equipment: Record<string, IWDItem | undefined> = {};
	const inv = player.inventory.items;
	equipment.eq_head = inv.find((i) => i.equipSlot === 'head');
	equipment.eq_neck = inv.find((i) => i.equipSlot === 'neck');
	equipment.eq_chest = inv.find((i) => i.equipSlot === 'chest');
	equipment.eq_hands = inv.find((i) => i.equipSlot === 'hands');
	equipment.eq_finger1 = inv.find((i) => i.equipSlot === 'finger-1');
	equipment.eq_finger2 = inv.find((i) => i.equipSlot === 'finger-2');
	equipment.eq_waist = inv.find((i) => i.equipSlot === 'waist');
	equipment.eq_legs = inv.find((i) => i.equipSlot === 'legs');
	equipment.eq_feet = inv.find((i) => i.equipSlot === 'feet');
	equipment.eq_trinket = inv.find((i) => i.equipSlot === 'trinket');
	equipment.eq_oneHanded = inv.find((i) => i.equipSlot === 'oneHanded');
	equipment.eq_offHand = inv.find((i) => i.equipSlot === 'offHand');
	equipment.eq_tool = inv.find((i) => i.equipSlot === 'tool');
	equipment.eq_quickslot = inv.find((i) => typeof i.quickSlot === 'number');
	equipment.eq_rune1 = inv.find((i) => i.runeSlot === 1);
	equipment.eq_rune2 = inv.find((i) => i.runeSlot === 2);
	equipment.eq_rune3 = inv.find((i) => i.runeSlot === 3);
	equipment.eq_rune4 = inv.find((i) => i.runeSlot === 4);

	// Stringify json
	const equipmentStringified: Record<string, string> = {};
	Object.keys(equipment).forEach((k) => {
		const v = equipment[k];
		if (v) {
			equipmentStringified[k] = JSON.stringify(v);
		}
	});

	// Check available methods and send POST to api
	try {
		await sendPostRequest(
			`/api/character/upload`,
			{
				...data,
				...rep,
				...equipmentStringified,
			},
			{
				Authorization: `Bearer ${token}`,
			},
		);
	} catch (e) {
		console.error(e);
		makePopup('Islebuilds upload failed', String(e));

		return;
	}

	if (!noMessage) {
		// Send a message in chat
		getWindow().Waddon.sendMessage(
			'Uploaded to Islebuilds',
			'color-yellowB',
			'info',
		);
	}

	if (!noOpen) {
		// Open tab
		openIslebuildsPage();
	}
};

const inventoryCache = {
	items: '[]',
	name: '',
};
const stashCache = {
	items: '[]',
	name: '',
};

export const uploadInventory = async () => {
	const player = getPlayer();
	if (!player?.name) return;

	const name = player.name;
	const items = player.inventory.items;

	uploadContainer(name, false, items);
};

export const uploadStash = async () => {
	const player = getPlayer();
	if (!player?.name) return;

	const name = player.account;
	const items = $('.uiStash').data('ui').items;

	uploadContainer(name, true, items);
};

export const uploadContainer = async (
	name: string,
	stash: boolean,
	items: any,
) => {
	// Check setting
	const syncInventory = settings.getValue('islebuilds.syncInventory');
	if (!syncInventory) return;

	// Check site
	if (!isValidHost()) return;

	// Check token
	const token = settings.getValue('islebuilds.authToken');
	if (!token || token === '') {
		return;
	}

	const itemsJson = JSON.stringify(items);

	// Unchanged
	const cache = stash ? stashCache : inventoryCache;
	if (itemsJson === cache.items && cache.name === name) {
		return;
	}

	cache.items = itemsJson;
	cache.name = name;

	try {
		await sendPostRequest(
			`/api/inventory`,
			{
				name: name,
				stash: stash,
				items: itemsJson,
			},
			{
				Authorization: `Bearer ${token}`,
			},
		);

		// console.log('posted items to islebuilds');
	} catch (e) {
		console.error('failed to post items to islebuilds:');
		console.error(e);
	}
};

const isValidHost = () => {
	if (getWindow().location.host !== 'play.isleward.com') {
		console.log('[islebuilds] skipping upload');
		return false;
	}
	return true;
};

export const getIslebuildsPagePath = (path: string) => {
	return `${ISLEBUILDS_WEB_URL}${path}`;
};

export const openIslebuildsPage = (url?: string) => {
	const player = getPlayer();

	if (url) {
		getWindow().open(`${ISLEBUILDS_WEB_URL}${url}`, '_blank');
	} else if (player) {
		getWindow().open(
			`${ISLEBUILDS_WEB_URL}/character/${player.name}`,
			'_blank',
		);
	} else {
		getWindow().open(ISLEBUILDS_WEB_URL, '_blank');
	}
};
