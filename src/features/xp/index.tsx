// TODO: Need to find a way to differentiate from XP from quests
/// (will come in big chunks, which makes the mobs remaining be more like quests remaining)

import React from 'react';
import { useEffect, useState } from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import hotkeys from '../../core/hotkeys';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import stateModule, { type GameState } from '../../core/state';
import type {
	IwdObject,
	IwdObjectsModule,
	IwdStatsCpn,
} from '../../isleward.types';
import { settings } from '../../settings';
import { getWindow } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import { XpPanel } from './XpPanel';

let objects: IwdObjectsModule;

let xpEvents: [number, number][] = [];
let calc = {
	total: -1,
	current: -1,
	remaining: -1,
	toLevel: -1,

	kills: -1,
};

// The first time we get our player object, read the total xp and level
const onGetObject = (obj: IwdObject) => {
	if (!obj.self) return;

	setTimeout(() => {
		const player = objects.objects.find((o) => o.self);
		if (!player?.stats) return;

		if (
			typeof player.stats.values.xpMax !== 'number' ||
			typeof player.stats.values.xp !== 'number'
		) {
			throw new Error('panic');
		}

		const needXp = player.stats.values.xpMax - player.stats.values.xp;

		calc = {
			total: player.stats.values.xpMax,
			current: player.stats.values.xp,
			remaining: needXp,
			toLevel: (player.level ?? 0) + 1,

			kills: -1,
		};

		notify();
	}, 0);
};

// Might be broken soon by iwd MR
const onGetDamage = (event: any) => {
	// Wait for objects/objects to process the update first
	setTimeout(() => {
		// Get player from objects/objects
		const player = objects.objects.find((o) => o.self);
		if (!player?.stats) {
			return;
		}

		// This property is set if the event is in plain text, not damage numbers
		if (!event.event) {
			return;
		}

		// Only look at our own XP popups
		if (event.id !== player.id) {
			return;
		}

		// Popup text should contain 'xp'
		if (event.text.indexOf('xp') === -1) {
			return;
		}

		if (
			typeof player.stats.values.xpMax !== 'number' ||
			typeof player.stats.values.xp !== 'number'
		) {
			throw new Error('panic');
		}

		const needXp = player.stats.values.xpMax - player.stats.values.xp;

		// Parse xp number from '+123 xp'
		const gotXp = parseInt(event.text.split(' ')[0].replace('+', ''));

		// Calculate number of kills required, with 1 decimal point
		let kills = needXp / gotXp;
		kills = Math.round(kills * 10) / 10;

		calc = {
			total: player.stats.values.xpMax,
			current: player.stats.values.xp,
			remaining: needXp,
			toLevel: (player.level ?? 0) + 1,

			kills: kills,
		};

		// Recording recent xp
		const now = Date.now();
		xpEvents.push([now, gotXp]);

		// Cleaning old events (1 hour retention)
		for (let i = 0; i < xpEvents.length; i++) {
			if (now - xpEvents[i]![0] > 60 * 60 * 1000) {
				xpEvents.shift();
				i--;
			}
		}

		notify();
	}, 0);
};

const onStateChanged = (state: GameState) => {
	if (state === 'game') {
		calc = {
			total: -1,
			current: -1,
			remaining: -1,
			toLevel: -1,

			kills: -1,
		};
		xpEvents = [];

		notify();
	}
};

type ListenerData = {
	xpEvents: typeof xpEvents;
	calc: typeof calc;
};
type Listener = (data: ListenerData) => void;
let listeners: Listener[] = [];
const subscribe = (cb: Listener) => {
	listeners.push(cb);
};
const unsubscribe = (cb: Listener) => {
	listeners = listeners.filter((l) => l !== cb);
};
const notify = () => {
	listeners.forEach((l) => l({ xpEvents, calc }));
};

export const useXpData = () => {
	const [data, setData] = useState<ListenerData>({ xpEvents, calc });

	useEffect(() => {
		const listener = (newData: ListenerData) => {
			setData(newData);
		};

		subscribe(listener);
		return () => {
			unsubscribe(listener);
		};
	});

	return data;
};

const init = async () => {
	objects = await requireAsync('js/objects/objects');
	const events = getWindow()?.addons?.events;
	if (!events) return;

	hotkeys.register('l', () =>
		settings.transactValue('xp.enabled', (x) => !x),
	);

	menu.addElement({
		key: 'xp panel',
		category: 'main',
		el: <ToggleSetting setting={'xp.enabled'}>xp panel</ToggleSetting>,
	});

	renderer.addElement('xp-panel', <XpPanel />);

	events.on('onGetObject', onGetObject);
	events.on('onGetDamage', onGetDamage);

	stateModule.subscribe(onStateChanged);
};

export default { init };
