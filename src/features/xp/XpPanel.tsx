import { Panel } from '@kckckc/isleward-util';
import classNames from 'classnames';
import { balanceConfig } from 'isleward-data';
import React, { useState } from 'react';
import { useXpData } from '.';
import { useGameState } from '../../core/state/useGameState';
import { useInterval } from '../../hooks/useInterval';
import { useSetting } from '../../hooks/useSetting';
import styles from './styles.module.css';

const decimal = (n: number, prec: number) => {
	return Math.round(n * Math.pow(10, prec)) / Math.pow(10, prec);
};

const TimeRow = ({ time, totalXp }: { time: number; totalXp: number }) => {
	return (
		<tr>
			<td className={styles.item}>{time} min:</td>
			<td className={styles.item}>{totalXp} xp</td>
			<td className={classNames(styles.item, styles.nopad)}>(</td>
			<td className={styles.item}>{decimal(totalXp / time, 1)} / min</td>
			<td className={classNames(styles.item, styles.nopad)}>)</td>
		</tr>
	);
};

export const XpPanel = () => {
	const state = useGameState();
	const { xpEvents, calc } = useXpData();

	const [, setRedraw] = useState({});

	// Redraw every 30s
	useInterval(() => {
		setRedraw({});
	}, 30 * 1000);

	const [enabled] = useSetting('xp.enabled');

	if (!enabled || state !== 'game') return null;

	// If max level or not in-game yet, hide
	const isMaxLevel = calc.toLevel === balanceConfig.maxLevel + 1;
	if (isMaxLevel || calc.toLevel < 0) return null;

	const percent = Math.round((calc.current / calc.total) * 100 * 10) / 10;

	const now = Date.now();

	const xp1 = xpEvents
		.filter((e) => now - e[0] <= 1000 * 60 * 1)
		.reduce((a, b) => a + b[1], 0);
	const xp5 = xpEvents
		.filter((e) => now - e[0] <= 1000 * 60 * 5)
		.reduce((a, b) => a + b[1], 0);
	const xp15 = xpEvents
		.filter((e) => now - e[0] <= 1000 * 60 * 15)
		.reduce((a, b) => a + b[1], 0);
	const xp60 = xpEvents
		.filter((e) => now - e[0] <= 1000 * 60 * 60)
		.reduce((a, b) => a + b[1], 0);

	return (
		<Panel
			saveAs="waddon.xp"
			header={
				calc.total !== -1 ? `XP: ${percent}% to ${calc.toLevel}` : 'XP'
			}
		>
			{calc.total !== -1 ? (
				<div style={{ margin: '4px', whiteSpace: 'nowrap' }}>
					XP: {calc.remaining}
					<br />
					{/* Potentially we could calculate the expected XP from a kill of a same-level enemy */}
					Kills: {calc.kills !== -1 ? calc.kills : 'waiting'}
					<br />
					<br />
					<table>
						<tbody>
							<TimeRow time={1} totalXp={xp1} />
							<TimeRow time={5} totalXp={xp5} />
							<TimeRow time={15} totalXp={xp15} />
							<TimeRow time={60} totalXp={xp60} />
						</tbody>
					</table>
				</div>
			) : (
				''
			)}
		</Panel>
	);
};
