import React, { useEffect, useState } from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import hotkeys from '../../core/hotkeys';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import stateModule, { type GameState } from '../../core/state';
import type {
	IwdObject,
	IwdObjectsModule,
	OnGetMapData,
} from '../../isleward.types';
import { settings } from '../../settings';
import { getWindow } from '../../util/getWindow';
import { getZoneName } from '../../util/getZoneName';
import { requireAsync } from '../../util/requireAsync';
import { MinimapPanel } from './MinimapPanel';
import { RULESET } from './rules';

// TODO:
// - Respect hidden rooms

let objects: IwdObjectsModule;

let zoneName: string;
let collisionMap: OnGetMapData['collisionMap'];
let map: OnGetMapData['map'];
let imageData: ImageData | null = null;

const onGetMap = (mapData: OnGetMapData) => {
	collisionMap = mapData.collisionMap;
	map = mapData.map;

	imageData = null;

	notify();
};

const onGetObject = (o: IwdObject) => {
	if (o.self && o.zoneName) {
		zoneName = o.zoneName;
	}

	notify();
};

const onStateChanged = (s: GameState) => {
	if (s !== 'game') {
		imageData = null;
	}

	notify();
};

export const drawMap = (context: CanvasRenderingContext2D) => {
	// Build base map image once
	if (!imageData) {
		const width = collisionMap.length;
		const height = collisionMap[0]!.length;

		context.clearRect(0, 0, width, height);
		imageData = context.getImageData(0, 0, width, height);
		const pixels = imageData.data;

		for (let i = 0; i < width; i++) {
			for (let j = 0; j < height; j++) {
				const offset = (j * width + i) * 4;

				const cell = map[i]![j]!;

				if (cell.length === 1 && (cell[0] === '0' || cell[0] === '')) {
					// #3c3f4c
					pixels[offset] = 60;
					pixels[offset + 1] = 63;
					pixels[offset + 2] = 76;
					pixels[offset + 3] = 255;

					// Transparent
					// pixels[offset] = 0;
					// pixels[offset + 1] = 0;
					// pixels[offset + 2] = 0;
					// pixels[offset + 3] = 0;
				} else if (collisionMap[i]![j]! === 0) {
					// #757b92
					pixels[offset] = 117;
					pixels[offset + 1] = 123;
					pixels[offset + 2] = 146;
					pixels[offset + 3] = 255;
				} else {
					// #2c2136
					pixels[offset] = 44;
					pixels[offset + 1] = 33;
					pixels[offset + 2] = 54;
					pixels[offset + 3] = 255;
				}
			}
		}
	}

	context.putImageData(imageData, 0, 0);
};

export const drawObjects = (context: CanvasRenderingContext2D) => {
	const minimapRules = settings.getValue('minimap.rules');

	for (const obj of objects.objects) {
		for (const rule of RULESET) {
			if (!minimapRules[rule.ruleId]) continue;

			if (rule.name) {
				if (
					!rule.name.some((n) => {
						if (n instanceof RegExp) {
							return (
								(obj.name || '').toLowerCase().match(n) !== null
							);
						} else {
							return (
								(obj.name || '').toLowerCase() ==
								n.toLowerCase()
							);
						}
					})
				) {
					continue;
				}
			}

			if (rule.isPlayer !== undefined) {
				const objIsPlayer = obj.aggro?.faction === 'players';
				if (objIsPlayer !== rule.isPlayer) {
					continue;
				}
			}

			if (rule.isSelf !== undefined) {
				if (!!obj.self !== rule.isSelf) {
					continue;
				}
			}

			if (rule.isRare !== undefined) {
				if (!!obj.isRare !== rule.isRare) {
					continue;
				}
			}

			if (typeof obj.x !== 'number' || typeof obj.y !== 'number') {
				continue;
			}

			context.fillStyle = rule.color;
			context.fillRect(
				obj.x - (rule.radius || 1) + 1,
				obj.y - (rule.radius || 1) + 1,
				rule.radius * 2 - 1,
				rule.radius * 2 - 1,
			);
		}
	}
};

export const getTitle = () => {
	return `${getZoneName(zoneName)} Map`;
};

export const getDimensions = () => {
	if (!collisionMap) return null;

	return {
		width: collisionMap.length,
		height: collisionMap[0]!.length,
	};
};

type Listener = () => void;
let listeners: Listener[] = [];
const subscribe = (cb: Listener) => {
	listeners.push(cb);
};
const unsubscribe = (cb: Listener) => {
	listeners = listeners.filter((l) => l !== cb);
};
const notify = () => {
	listeners.forEach((l) => l());
};
export const useMinimapData = () => {
	const [data, setData] = useState({});

	useEffect(() => {
		const cb = () => {
			setData({});
		};

		subscribe(cb);
		return () => unsubscribe(cb);
	});

	// Useless for now
	return data;
};

const onMinimapClicked = (data: { x: number; y: number }) => {
	const enabled = settings.getValue('minimap.logObjects');
	if (!enabled) return;

	const maxDistance = 5;
	const list = objects.objects.filter((o) => {
		if (typeof o.x !== 'number' || typeof o.y !== 'number') return;

		const dx = Math.abs(o.x - data.x);
		if (dx < maxDistance) {
			const dy = Math.abs(o.y - data.y);
			if (dy < maxDistance) return true;
		}

		return false;
	});
	if (list.length === 1) console.log(list[0]);
	else if (list.length > 1) console.log(list);
};

const init = async () => {
	objects = await requireAsync('js/objects/objects');
	const events = getWindow()?.addons?.events;

	hotkeys.register('m', () =>
		settings.transactValue('minimap.enabled', (x) => !x),
	);

	menu.addElement({
		key: 'minimap panel',
		category: 'main',
		el: (
			<ToggleSetting setting={'minimap.enabled'}>
				minimap panel
			</ToggleSetting>
		),
	});

	renderer.addElement('minimap-panel', <MinimapPanel />);

	events?.on('onGetObject', onGetObject);
	events?.on('onGetMap', onGetMap);
	events?.on('waddon:onMinimapClicked', onMinimapClicked);

	events?.on('onRezone', () => {
		notify();
	});

	stateModule.subscribe(onStateChanged);
};

export default { init };
