import { Button, Panel } from '@kckckc/isleward-util';
import React, { useState } from 'react';
import { useInterval } from '../../hooks/useInterval';
import { useSetting } from '../../hooks/useSetting';
import farmModule from './index';
import styles from './styles.module.css';

const decimal = (n: number, prec = 2) => {
	return Math.round(n * Math.pow(10, prec)) / Math.pow(10, prec);
};

export const FarmPanel: React.FC = () => {
	const { a, b } = farmModule.useFarmStats();

	const [show] = useSetting('farm.enabled');

	// Set this every 30s to redraw time-based stuff without new data
	const [, setRedraw] = useState({});
	useInterval(() => {
		setRedraw({});
	}, 30 * 1000);

	const minsPassed = (Date.now() - (a?.time ?? Date.now())) / (1000 * 60);

	if (!show) return null;

	const isStarted = a && b;
	const contents = isStarted ? (
		<>
			<div className={styles.timediff}>
				{decimal((Date.now() - a.time) / (1000 * 60), 2)} mins total{' '}
				{b.slots > -1 ? `(${b.slots} slots open)` : ''}
			</div>

			<div className={styles.diff}>
				{Object.entries(a.items).map(([key, valA]) => {
					const valB = b.items[key] ?? 0;

					const diff = valB - valA;
					const perMin = diff / minsPassed;
					let prefix = '';
					if (diff > 0) {
						prefix = '+';
					}

					if (diff != 0) {
						return (
							<div key={key}>
								{key}: {prefix}
								{decimal(diff, 2)} ({prefix}
								{decimal(perMin, 2)}/min)
							</div>
						);
					} else {
						return null;
					}
				})}
				{Object.entries(b.items).map(([key, valB]) => {
					let valA = a.items[key];
					if (valA) return; // only add entries that haven't been done yet
					valA = 0;

					const diff = valB - valA;
					const perMin = diff / minsPassed;
					let prefix = '';
					if (diff > 0) {
						prefix = '+';
					}

					if (diff != 0) {
						return (
							<div key={key}>
								{key}: {prefix}
								{decimal(diff, 2)} ({prefix}
								{decimal(perMin, 2)}/min)
							</div>
						);
					} else {
						return null;
					}
				})}
			</div>
		</>
	) : (
		<div className={styles.timediff}>
			Or press / and run Farm: Start Tracking
		</div>
	);

	return (
		<Panel className={styles.container} saveAs="waddon.farm" header="Farm">
			<div className={styles.buttons}>
				<Button onClick={() => farmModule.updateInitial()}>
					{a ? 're' : ''}start
				</Button>
			</div>
			{contents}
		</Panel>
	);
};
