import React, { useEffect, useState } from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import slash from '../../core/slash';
import { getPlayer, getWindow } from '../../util/getWindow';
import { FarmPanel } from './FarmPanel';

interface FarmSnapshot {
	slots: number;
	time: number;
	items: Record<string, number>;
}

let snapshotInitial: FarmSnapshot | null = null;
let snapshotCurrent: FarmSnapshot | null = null;

const getSnapshot = (): FarmSnapshot | null => {
	const player = getPlayer();
	if (!player) return null;
	const items = player?.inventory?.items;

	const slots = 50 - items.filter((i) => !i.eq).length;

	const count: Record<string, number> = {};

	items.forEach((i) => {
		if (!i.name) return;

		if (i.material || i.quest || i.type === "Gambler's Card") {
			count[i.name] = (count[i.name] ?? 0) + (i.quantity ?? 1);
		} else {
			count['(Other Items)'] = (count['(Other Items)'] ?? 0) + 1;
		}
	});

	const time = Date.now();

	return {
		slots,
		time,
		items: count,
	};
};

const updateInitial = () => {
	snapshotInitial = getSnapshot();
	notify();
};
const updateCurrent = () => {
	snapshotCurrent = getSnapshot();
	notify();
};

const getInitial = () => {
	return snapshotInitial;
};
const getCurrent = () => {
	return snapshotCurrent;
};

const onGetItems = () => {
	setTimeout(() => updateCurrent(), 0);
};
const onDestroyItems = () => {
	setTimeout(() => updateCurrent(), 0);
};

let listeners: (() => void)[] = [];
const subscribe = (listener: () => void) => {
	listeners.push(listener);
};
const unsubscribe = (listener: () => void) => {
	listeners = listeners.filter((l) => l !== listener);
};
const notify = () => {
	listeners.forEach((l) => l());
};

const useFarmStats = () => {
	const [data, setData] = useState({
		a: snapshotInitial,
		b: snapshotCurrent,
	});

	useEffect(() => {
		const listener = () => {
			setData({
				a: snapshotInitial,
				b: snapshotCurrent,
			});
		};

		subscribe(listener);
		return () => {
			unsubscribe(listener);
		};
	}, []);

	return data;
};

const init = () => {
	const events = getWindow()?.addons?.events;

	slash.registerAction({
		id: 'farm:starttracking',
		name: 'Farm: Start Tracking',
		callback: () => {
			updateInitial();
		},
	});

	menu.addElement({
		key: 'farm panel',
		category: 'main',
		el: <ToggleSetting setting={'farm.enabled'}>farm panel</ToggleSetting>,
	});

	renderer.addElement('farm-panel', <FarmPanel />);

	events?.on('onGetItems', onGetItems);
	events?.on('onDestroyItems', onDestroyItems);
};

export default {
	init,
	updateInitial,
	updateCurrent,
	getInitial,
	getCurrent,
	useFarmStats,
};
