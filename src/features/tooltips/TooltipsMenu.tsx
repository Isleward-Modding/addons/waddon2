import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';

export const TooltipsMenu: React.FC = () => {
	return (
		<>
			<ToggleSetting setting={'tooltips.enabled'}>
				enable tooltips
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.cardSetSize'}>
				show card set size
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.shortenStatNames'}>
				shorten stat names
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.maxLevel'}>
				always show roll at max level
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.maxLevel23'}>
				use level 23 as max level
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.stats'}>
				show item stat rolls
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.spells'}>
				show spell rolls
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.showPercent'}>
				show roll percents
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.showRange'}>
				show roll ranges
			</ToggleSetting>
			<ToggleSetting setting={'tooltips.perfection.showOverall'}>
				show overall perfection
			</ToggleSetting>
		</>
	);
};
