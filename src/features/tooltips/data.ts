const STAT_TRANSLATIONS: Record<string, string> = {
	vit: 'vitality',
	regenHp: 'health regeneration',
	manaMax: 'maximum mana',
	regenMana: 'mana regeneration',
	str: 'strength',
	int: 'intellect',
	dex: 'dexterity',
	armor: 'armor',

	blockAttackChance: 'chance to block attacks',
	blockSpellChance: 'chance to block spells',

	dodgeAttackChance: 'chance to dodge attacks',
	dodgeSpellChance: 'chance to dodge spells',

	addCritChance: 'global crit chance',
	addCritMultiplier: 'global crit multiplier',
	addAttackCritChance: 'attack crit chance',
	addAttackCritMultiplier: 'attack crit multiplier',
	addSpellCritChance: 'spell crit chance',
	addSpellCritMultiplier: 'spell crit multiplier',
	magicFind: 'increased item quality',
	itemQuantity: 'increased item quantity',
	sprintChance: 'sprint chance',
	allAttributes: 'to all attributes',
	xpIncrease: 'additional xp per kill',
	lvlRequire: 'level requirement reduction',

	elementArcanePercent: 'increased arcane damage',
	elementFrostPercent: 'increased frost damage',
	elementFirePercent: 'increased fire damage',
	elementHolyPercent: 'increased holy damage',
	elementPoisonPercent: 'increased poison damage',
	physicalPercent: 'increased physical damage',

	elementPercent: 'increased elemental damage',
	spellPercent: 'increased spell damage',

	elementAllResist: 'all resistance',
	elementArcaneResist: 'arcane resistance',
	elementFrostResist: 'frost resistance',
	elementFireResist: 'fire resistance',
	elementHolyResist: 'holy resistance',
	elementPoisonResist: 'poison resistance',

	attackSpeed: 'attack speed',
	castSpeed: 'cast speed',

	addAttackDamage: 'added attack damage',
	addSpellDamage: 'added spell damage',

	lifeOnHit: 'life gained on dealing physical damage',

	auraReserveMultiplier: 'aura mana reservation multiplier',

	//This stat is used for gambling when you can't see the stats
	stats: 'stats',

	//Fishing
	weight: 'lb',
	//Rods
	catchChance: 'extra catch chance',
	catchSpeed: 'faster catch speed',
	fishRarity: 'higher fish rarity',
	fishWeight: 'increased fish weight',
	fishItems: 'extra chance to hook items',
};

const SHORT_STAT_TRANSLATIONS: Record<string, string> = {
	vit: 'vitality',
	regenHp: 'hp regen',
	manaMax: 'max mana',
	regenMana: 'mana regen',
	str: 'strength',
	int: 'intellect',
	dex: 'dexterity',
	armor: 'armor',

	blockAttackChance: 'attack block',
	blockSpellChance: 'spell block',

	dodgeAttackChance: 'attack dodge',
	dodgeSpellChance: 'spell dodge',

	addCritChance: 'global crit %',
	addCritMultiplier: 'global crit mult',
	addAttackCritChance: 'attack crit %',
	addAttackCritMultiplier: 'attack crit mult',
	addSpellCritChance: 'spell crit %',
	addSpellCritMultiplier: 'spell crit mult',
	magicFind: 'item quality',
	itemQuantity: 'item quantity',
	sprintChance: 'sprint chance',
	allAttributes: 'all attributes',
	xpIncrease: 'xp increase',
	lvlRequire: 'lvl req reduction',

	elementArcanePercent: 'arcane damage',
	elementFrostPercent: 'frost damage',
	elementFirePercent: 'fire damage',
	elementHolyPercent: 'holy damage',
	elementPoisonPercent: 'poison damage',
	physicalPercent: 'physical damage',

	elementPercent: 'elemental damage',
	spellPercent: 'spell damage',

	elementAllResist: 'all res',
	elementArcaneResist: 'arcane res',
	elementFrostResist: 'frost res',
	elementFireResist: 'fire res',
	elementHolyResist: 'holy res',
	elementPoisonResist: 'poison res',

	attackSpeed: 'attack speed',
	castSpeed: 'cast speed',

	addAttackDamage: 'added attack damage',
	addSpellDamage: 'added spell damage',

	lifeOnHit: 'life gained on hit',

	auraReserveMultiplier: 'aura mana reservation multiplier',

	//This stat is used for gambling when you can't see the stats
	stats: 'stats',

	//Fishing
	weight: 'lb',
	//Rods
	catchChance: 'extra catch chance',
	catchSpeed: 'faster catch speed',
	fishRarity: 'higher fish rarity',
	fishWeight: 'increased fish weight',
	fishItems: 'extra chance to hook items',
};

const translate = (stat: string, settings = { shortenStatNames: false }) => {
	if (settings.shortenStatNames) {
		return SHORT_STAT_TRANSLATIONS[stat] ?? STAT_TRANSLATIONS[stat] ?? stat;
	} else {
		return STAT_TRANSLATIONS[stat] ?? stat;
	}
};

const percentageStats = [
	'addCritChance',
	'addCritemultiplier',
	'addAttackCritChance',
	'addAttackCritemultiplier',
	'addSpellCritChance',
	'addSpellCritemultiplier',
	'sprintChance',
	'xpIncrease',
	'blockAttackChance',
	'blockSpellChance',
	'dodgeAttackChance',
	'dodgeSpellChance',
	'attackSpeed',
	'castSpeed',
	'itemQuantity',
	'magicFind',
	'catchChance',
	'catchSpeed',
	'fishRarity',
	'fishWeight',
	'fishItems',
];

const stringifyStatValue = (statName: string, statValue: number) => {
	let res = statValue;
	if (statName.indexOf('CritChance') > -1) res = res / 20;

	let resStr = '' + res;

	if (
		percentageStats.includes(statName) ||
		statName.indexOf('Percent') > -1 ||
		(statName.indexOf('element') === 0 && statName.indexOf('Resist') === -1)
	)
		resStr += '%';

	return resStr;
};

export { STAT_TRANSLATIONS, translate, stringifyStatValue };
