import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import menu from '../../core/menu';
import stateModule, { type GameState } from '../../core/state';
import type { IwdChatMessage } from '../../isleward.types';
import { settings } from '../../settings';
import { getPlayer, getWindow } from '../../util/getWindow';

const buffer: IwdChatMessage[] = [];
const rollingBuffer: IwdChatMessage[] = [];
const keepRolling = 20;

const tryReplay = () => {
	if (!settings.getValue('qol.chatReplay')) return;

	if (!buffer.length || !getPlayer()) {
		return;
	}
	const len = buffer.length;

	const ui = $('.uiMessages').data('ui');
	if (!ui) return;

	ui.onGetMessages({
		messages: [
			{
				class: 'color-yellowB',
				type: 'info',
				message: `--- begin replay of ${len} ---`,
			},
		],
	});

	while (buffer.length) {
		const next = buffer.shift();

		ui.onGetMessages({
			messages: [next],
		});
	}

	ui.onGetMessages({
		messages: [
			{
				class: 'color-yellowB',
				type: 'info',
				message: `--- end replay of ${len} ---`,
			},
		],
	});
};

const onGetMessages = ({ messages }: { messages: IwdChatMessage[] }) => {
	if (!settings.getValue('qol.chatReplay')) return;

	if (stateModule.get() === 'game') {
		if (!Array.isArray(messages)) {
			messages = [messages];
		}
		messages.forEach((m) => rollingBuffer.push(m));

		// Limit length
		while (rollingBuffer.length > keepRolling) {
			rollingBuffer.shift();
		}

		tryReplay();
	} else {
		// If anything was added to the rolling buffer, it was while playing
		// We're now not playing, so if there's anything there,
		// add it to the buffer asap
		while (rollingBuffer.length) {
			const item = rollingBuffer.shift();
			if (item) {
				buffer.push(item);
			}
		}

		if (!Array.isArray(messages)) {
			messages = [messages];
		}

		messages.forEach((m) => {
			buffer.push(m);
		});
	}
};

const onStateChanged = (state: GameState) => {
	if (state === 'game') {
		tryReplay();
	}
};

const init = () => {
	const events = getWindow()?.addons?.events;

	menu.addElement({
		key: 'chat replay',
		category: 'qol',
		el: (
			<ToggleSetting setting={'qol.chatReplay'}>
				chat replay [experimental]
			</ToggleSetting>
		),
	});

	events?.on('onGetMessages', onGetMessages);

	stateModule.subscribe(onStateChanged);
};

export default { init };
