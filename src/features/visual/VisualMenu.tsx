import { CM } from '@kckckc/isleward-util';
import React from 'react';
import { VISUAL_INITIAL } from '.';
import { useSetting } from '../../hooks/useSetting';

export const VisualMenu: React.FC = () => {
	const [opts, setOpts] = useSetting('renderer');

	return (
		<>
			<CM.Button
				onClick={() => {
					setOpts(VISUAL_INITIAL);
				}}
			>
				reset
			</CM.Button>
			<CM.Checkbox
				checked={opts.showPlayer}
				onChange={() => {
					setOpts((prev) => ({
						...prev,
						showPlayer: !prev.showPlayer,
					}));
				}}
			>
				show player
			</CM.Checkbox>
			<CM.Checkbox
				checked={opts.showCursor}
				onChange={() => {
					setOpts((prev) => ({
						...prev,
						showCursor: !prev.showCursor,
					}));
				}}
			>
				show cursor
			</CM.Checkbox>
			<CM.Expand label="isleward uis">
				{Object.keys(opts.uis).map((f) => {
					return (
						<CM.Checkbox
							key={f}
							checked={!!opts.uis[f]}
							onChange={() => {
								setOpts((prev) => ({
									...prev,
									uis: {
										...prev.uis,
										[f]: !prev.uis[f],
									},
								}));
							}}
						>
							{f.slice(2)}
						</CM.Checkbox>
					);
				})}
			</CM.Expand>
			<CM.Expand label="renderer layers">
				{Object.keys(opts.layers).map((f) => {
					return (
						<CM.Checkbox
							key={f}
							checked={!!opts.layers[f]}
							onChange={() => {
								setOpts((prev) => ({
									...prev,
									layers: {
										...prev.layers,
										[f]: !prev.layers[f],
									},
								}));
							}}
						>
							{f} layer
						</CM.Checkbox>
					);
				})}
			</CM.Expand>
		</>
	);
};
