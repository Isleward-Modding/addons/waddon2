import { CM, Panel } from '@kckckc/isleward-util';
import React, { useState } from 'react';
import { TICK_DURATION } from '../../config';
import { useInterval } from '../../hooks/useInterval';
import { useSetting } from '../../hooks/useSetting';
import timers from '.';

const prettifyDuration = (timestamp: number, shouldPrettify: boolean) => {
	const until = timestamp - Date.now();

	if (until < 0) {
		const ago = Math.abs(until);
		const seconds = Math.ceil(ago / 1000);
		const minutes = Math.floor(seconds / 60);
		const remainder = '' + (seconds % 60);

		if (minutes > 59) return 'Unknown';

		if (!shouldPrettify) {
			return Math.ceil(ago / TICK_DURATION);
		}

		return `Up ${minutes}:${remainder.padStart(2, '0')} ago`;
	}

	if (!shouldPrettify) {
		return Math.ceil(until / TICK_DURATION);
	}

	const seconds = Math.ceil(until / 1000);
	const minutes = Math.floor(seconds / 60);
	const remainder = '' + (seconds % 60);

	return `In ${minutes}:${remainder.padStart(2, '0')}`;
};

export const TimersPanel = () => {
	const timings = timers.useTimings();

	const [shouldPrettify, setShouldPrettify] = useSetting(
		'timers.prettifyTicks',
	);

	const [cachedTimings] = useSetting('timers.cache');

	const [show] = useSetting('timers.enabled');

	const [, rerender] = useState({});

	useInterval(() => {
		rerender({});
	}, TICK_DURATION);

	if (!show) return null;

	return (
		<Panel
			saveAs="waddon.timers"
			header="Timers"
			contextMenu={
				<CM>
					<CM.Checkbox
						checked={shouldPrettify}
						onChange={() => {
							setShouldPrettify((x) => !x);
						}}
					>
						Prettify ticks
					</CM.Checkbox>
				</CM>
			}
		>
			<div
				style={{
					display: 'flex',
					flexDirection: 'column',
					margin: '8px',
					rowGap: '4px',
				}}
			>
				{timers.config.map((timer) => {
					const timing = timings[timer.id] ?? cachedTimings[timer.id];

					let inner;
					if (typeof timing === 'undefined') {
						inner = 'Unknown';
					} else {
						inner = prettifyDuration(timing, shouldPrettify);
					}

					return (
						<div key={timer.id}>
							{timer.name}: {inner}
						</div>
					);
				})}
			</div>
		</Panel>
	);
};
