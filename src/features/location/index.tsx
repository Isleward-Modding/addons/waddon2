import { Panel } from '@kckckc/isleward-util';
import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import { useGameState } from '../../core/state/useGameState';
import { useIwdPlayer } from '../../hooks/useIwdPlayer';
import { useSetting } from '../../hooks/useSetting';
import { getZoneName } from '../../util/getZoneName';

const LocationPanel: React.FC = () => {
	const player = useIwdPlayer();
	const state = useGameState();
	const [enabled] = useSetting('location.enabled');

	if (!enabled || !player || state !== 'game') return null;

	return (
		<Panel
			className="location"
			saveAs="waddon.location"
			header={getZoneName(player?.zoneName ?? 'location')}
		>
			<p
				style={{
					whiteSpace: 'nowrap',
					width: '13ch',
					textAlign: 'center',
				}}
			>
				X: {player?.x || '???'} Y: {player?.y || '???'}
			</p>
		</Panel>
	);
};

const init = () => {
	menu.addElement({
		key: 'location panel',
		category: 'main',
		el: (
			<ToggleSetting setting={'location.enabled'}>
				location panel
			</ToggleSetting>
		),
	});

	renderer.addElement('location-panel', <LocationPanel />);
};

export default { init };
