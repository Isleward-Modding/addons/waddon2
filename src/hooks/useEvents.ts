import { getWindow } from '../util/getWindow';

export const useEvents = () => {
	return getWindow()?.addons?.events;
};
