import { useEffect, useState } from 'react';
import { requireAsync } from '../util/requireAsync';

export const useRequire = <T>(path: string): T | null => {
	const [data, setData] = useState<T | null>(null);

	useEffect(() => {
		const doRequire = async () => {
			const result = await requireAsync(path);
			setData(result);
		};

		doRequire();
	}, [path]);

	return data;
};
