import { getWindow } from './getWindow';

// RequireJS require promise wrapper
export const requireAsync = async (path: string): Promise<any> => {
	return new Promise((res) => {
		getWindow().require([path], (m: any) => {
			res(m);
		});
	});
};
