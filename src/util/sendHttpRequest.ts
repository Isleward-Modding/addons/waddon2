import { ISLEBUILDS_API_URL } from '../config';
import { getWindow } from './getWindow';

// Certain environments (userscript manager / electron client / AddonManager) have different access to making requests

const getApiUrl = () => {
	//@ts-expect-error not declared
	const override = getWindow().WADDON_ISLEBUILDS_API_URL;
	if (typeof override === 'string') {
		return override as string;
	} else {
		return ISLEBUILDS_API_URL;
	}
};

export const sendPostRequest = (path: string, data: any, headers = {}) => {
	return new Promise<string>((resolve, reject) => {
		try {
			//@ts-expect-error gm_ functions not defined
			if (typeof GM_xmlhttpRequest !== 'undefined') {
				//@ts-expect-error gm_ functions not defined
				GM_xmlhttpRequest({
					url: `${getApiUrl()}${path}`,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						...headers,
					},
					data: JSON.stringify(data),
					onload: (response: any) => {
						if (response.status === 200) {
							resolve(response.responseText as string);
						} else {
							reject(response.statusText);
						}
					},
					onerror: reject,
				});
			} else if (typeof fetch !== 'undefined') {
				fetch(`${getApiUrl()}${path}`, {
					method: 'post',
					headers: {
						'Content-Type': 'application/json',
						...headers,
					},
					body: JSON.stringify(data),
				})
					.then((res) => {
						if (res.status !== 200)
							throw new Error(res.status + '');

						return res.text();
					})
					.then(resolve)
					.catch(reject);
			} else {
				const msg = `Waddon -- Error posting to islebuilds api: Missing GM_xmlhttpRequest or fetch.`;
				reject(msg);
			}
		} catch (e) {
			reject(e);
		}
	});
};

export const sendGetRequest = (path: string, headers = {}) => {
	return new Promise<string>((resolve, reject) => {
		try {
			//@ts-expect-error gm_ functions not defined
			if (typeof GM_xmlhttpRequest !== 'undefined') {
				//@ts-expect-error gm_ functions not defined
				GM_xmlhttpRequest({
					url: `${getApiUrl()}${path}`,
					method: 'GET',
					headers,
					onload: (response: any) => {
						resolve(response.responseText as string);
					},
					onerror: reject,
				});
			} else if (typeof fetch !== 'undefined') {
				fetch(`${getApiUrl()}${path}`, { headers })
					.then((res) => {
						if (res.status !== 200)
							throw new Error(res.status + '');

						return res.text();
					})
					.then(resolve)
					.catch(reject);
			} else {
				const msg = `Waddon -- Error posting to islebuilds api: Missing GM_xmlhttpRequest or fetch.`;
				reject(msg);
			}
		} catch (e) {
			reject(e);
		}
	});
};
