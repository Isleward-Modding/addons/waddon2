# IsleWaddon

**v3.0.0 Released: See CHANGELOG.md for details**

General purpose addon for Isleward.
Features a minimap, boss timers, Islebuilds uploads, item stat ranges, XP tracker, other QoL, and more.

This project was previously called Waddon2, and started as a rewrite of Polfy's original IsleWaddon.
The project has evolved a lot since then and has been renamed Waddon, as of v3.0.0.

**[Link to ViolentMonkey](https://violentmonkey.github.io/)**

**[Link to Userscript](https://gitlab.com/Isleward-Modding/addons/waddon/-/raw/master/dist/waddon.user.js)**

**[Video guide for installation](https://youtu.be/-JW8TD6y8W4)**

## Usage

Press `N` to open the main addon settings panel.
You can enable various panels and features from here.
There are some key combinations like `N+M` to toggle the minimap, `N+T` to toggle timers, and `N+L` to open the XP/levelling panel.
Right-clicking on the header of a panel opens the settings menu for that feature.

v3.0.0 changed the main menu.
Press `B` to open the old menu.
This menu will be removed once everything is moved to the new menu.

Press `/` to open the action bar and search for various features.

## Install

### Browser

Install a userscript manager for your browser.
[ViolentMonkey](https://violentmonkey.github.io/) is recommended.

Open the raw userscript [here](https://gitlab.com/Isleward-Modding/addons/waddon/-/raw/master/dist/waddon.user.js).
Your userscript manager should open a page to automatically install the addon.
If it doesn't open automatically, copy and paste the entire file into a new userscript.
Afterwards, refresh Isleward.

### Desktop

Download the `waddon.user.js` file and place it in the desktop client's addons folder.
See [here](https://gitlab.com/Isleward/desktop-client/-/tree/master#how-do-i-load-addons).
A restart of the client is probably needed afterwards.

### Development

Run `pnpm dev` to start Rollup with `--watch`.
Use the proxy script from `scripts/dev.user.js` (works best with a `file://` url and TamperMonkey) which will load the actual userscript from the filesystem.
This way, the userscript loader will always fetch the new version of the file from the filesystem in 1 refresh.
I sometimes use Tampermonkey for the proxy/development script, but ViolentMonkey is more consistent and clean when it comes to installing and updating.

`pnpm build` will build the `waddon.meta.js` and `waddon.user.js` files.
The `waddon.meta.js` file contains only the userscript header and is used to check if updates are available without downloading the full code from `waddon.user.js`.

Automatic CI builds for the master branch are found in `dist/`.

## Features

### Available
* Options Menu
* Minimap
* Boss Timers
* Islebuilds Uploader
* XP Meter
* Farming Tracker
* Visual Controls
    * Player
    * Cursor
    * Isleward UIs
    * Renderer Layers
* Tooltips
    * Card Set Sizes
    * Weapon/Rune Rolls Perfection
* Coordinates

### Might be broken
* Extended Player Context Menu
    * Whisper
    * Invite to Party

### Maybe in the future
* CRON timers
* Toggle timers on/off
* Timer callouts
* Minimap nameplates
* Persist timers in localstorage
* Party ready check
* Passive Templates (rewrite)
* DPS meter
* Reputation meter
* Farm/luck session
* Combat Log
* Event log exporter
* Chat Timestamps
* Rune/weapon damage/item roll/aug perfection
* Always Show Price
* More minimap options (zoom on character, showing explored areas only, etc)
* Color customization/use iwd color palette by default
* More tooltip customization (implicitStats, stats, augs)
* UI snapping to window edges/other Isleward or Waddon UIs
* Inventory/stash search like DIM
* Save last login time (in localstorage?) and display when daily rewards are available (to log in on the right character for mail)
* Log all events to file + log viewer (separate project -- gw2 arcdps reports)

## Thanks

Thanks to Polfy and Qndel for [their past Isleward addons](https://gitlab.com/Isleward-Modding/addons).
Some of the features in this addon are rewrites of their ideas.

The [original minimap addon](https://gitlab.com/Isleward-Modding/addons/map-addon) was written by Vildravn.

And thanks for playing Isleward and using the addon!
