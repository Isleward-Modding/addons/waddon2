import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import terser from '@rollup/plugin-terser';
import typescriptPlugin from '@rollup/plugin-typescript';
import 'dotenv/config';
import styles from 'rollup-plugin-styles';
import metablock from 'rollup-plugin-userscript-metablock';
import typescript from 'typescript';
import waddonFeaturesList from './scripts/waddon-features-list.js';

import featureList from './features.js';
// eslint-disable-next-line no-undef
const extraFeatureList = (process.env.EXTRA_FEATURES ?? '')
	.split(',')
	.map((i) => i.trim())
	.filter((i) => i.length > 0);
const fullFeatureList = [...featureList, ...extraFeatureList];

// eslint-disable-next-line no-undef
const isProduction = process.env.NODE_ENV === 'production';
// eslint-disable-next-line no-undef
const targetDir = process.env.BUILD_DIR ?? 'build';

import pkg from './package.json' with { type: 'json' };

export default {
	input: 'src/index.ts',
	output: [
		{
			name: 'waddon',
			file: targetDir + '/waddon.user.js',
			format: 'iife',
			sourcemap: isProduction ? false : 'inline',
		},
		{
			name: 'waddon.meta',
			file: targetDir + '/waddon.meta.js',
			sourcemap: false,
		},
	],
	watch: {
		chokidar: {
			paths: 'src/**',
		},
	},
	plugins: [
		styles({
			modules: true,
		}),
		waddonFeaturesList(fullFeatureList),
		replace({
			values: {
				'process.env.NODE_ENV': JSON.stringify('production'),
				ENVIRONMENT: JSON.stringify('production'),
				__ADDONVERSION__: JSON.stringify(pkg.version),
			},
			preventAssignment: true,
		}),
		nodeResolve({ extensions: ['.js', '.ts', '.tsx'] }),
		typescriptPlugin({ typescript }),
		commonjs({
			include: ['node_modules/**'],
			exclude: ['node_modules/process-es6/**'],
		}),
		babel({ babelHelpers: 'bundled' }),
		isProduction ? terser() : null,
		metablock({
			file: './meta.json',
			override: {
				version: pkg.version,
			},
		}),
		{
			name: 'meta only',

			renderChunk(code, chunk, options) {
				if (options.name !== 'waddon.meta') return;

				return code.split('==/UserScript==\n')[0] + '==/UserScript==\n';
			},
		},
	],
};
