const generateEnabled = (list) => {
	let file = '';

	list.forEach((f, i) => {
		file += `import f${i} from './src/features/${f}';`;
	});

	file += `const list = [${list.map((_, i) => 'f' + i).join(', ')}];`;
	file += 'export default list;';

	return file;
};

export default (list) => {
	return {
		name: 'waddon-enabled',
		resolveId(source) {
			if (source === 'waddon-enabled') {
				return source;
			}
			return null;
		},
		load(id) {
			if (id === 'waddon-enabled') {
				return generateEnabled(list);
			}
			return null;
		},
	};
};
