// @ts-check

import { fixupPluginRules } from '@eslint/compat';
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import react from 'eslint-plugin-react';
import reactHooks from 'eslint-plugin-react-hooks';
import prettierRecommended from 'eslint-plugin-prettier/recommended';
import globals from 'globals';

export default tseslint.config(
	eslint.configs.recommended,

	// TODO: switch to strict/stylistic once lints are caught up
	...tseslint.configs.recommended,
	// ...tseslint.configs.strict,
	// ...tseslint.configs.stylistic,

	// eslint-plugin-react
	{
		files: ['**/*.{js,mjs,cjs,jsx,mjsx,ts,tsx,mtsx}'],
		...react.configs.flat.recommended,
		languageOptions: {
			...react.configs.flat.recommended.languageOptions,
			globals: {
				...globals.browser,
			},
		},
	},

	// eslint-plugin-react-hooks
	{
		plugins: {
			// @ts-expect-error eslint-plugin-react-hooks isn't updated
			'react-hooks': fixupPluginRules(reactHooks),
		},
		// @ts-expect-error eslint-plugin-react-hooks isn't updated
		rules: {
			...reactHooks.configs.recommended.rules,
		},
	},

	// eslint-plugin-prettier
	prettierRecommended,

	{
		ignores: ['node_modules/**/*', 'dist/', 'build/'],
	},
	{
		rules: {
			'prettier/prettier': ['error', { endOfLine: 'auto' }],
		},
	},
);
